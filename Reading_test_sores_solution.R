#Load the training and testing sets
pisa_training <- read.csv("pisa2009train.csv")
pisa_testing <-read.csv("pisa2009test.csv")

#Summary the data
str(pisa_training)
#what is the average reading test score of males?
tapply(pisa_training$readingScore, pisa_training$male, mean)

#Linear regression discards observations with missing data. Remove observations with 
#any missing value from pisaTrain and pisaTest
pisaTrain = na.omit(pisaTrain)
pisaTest = na.omit(pisaTest)

#To build a linear regression model (call it lmScore) using the training set to predict 
#readingScore using all the remaining variables.
lmScore <- lm(readingScore ~ ., data=pisaTrain)

#What is the Multiple R-squared value of lmScore on the training set?
summary(lmScore)

#What is the training-set root-mean squared error (RMSE) of lmScore?
SSE <- sum(lmScore$residuals^2)
sqrt(SSE/nrow(pisaTrain))

#Predict on unseen Data
predTest = predict(lmScore, newdata=pisaTest)

#What is the sum of squared errors (SSE) of lmScore on the testing set?
TSSE <- sum((predTest-pisaTest$readingScore)^2)
TSSE

#What is the root-mean squared error (RMSE) of lmScore on the testing set?
sqrt(mean((predTest-pisaTest$readingScore)^2))

#What is the predicted test score used in the baseline model?
baseline = mean(pisaTrain$readingScore)

#What is the sum of squared errors of the baseline model on the testing set?
TSST <- sum((baseline-pisaTest$readingScore)^2)
TSST

#What is the test-set R-squared value of lmScore?
1-(TSSE/TSST)
